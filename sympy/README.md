# README.md

This directory is about Sympy and its tutorial.

## Notes
While the installation guide tells me to install sympy using "conda", I use PIP because I don't have conda environment.

# References

* [SymPy Homepage](https://www.sympy.org/en/index.html)
* [SymPy Documentation](https://docs.sympy.org/latest/index.html)
* [SymPy 1.12 Tutorial](https://docs.sympy.org/latest/tutorials/intro-tutorial/index.html)
