# Project dm

## What Is This?

This public project hosts a collection of scripts, such as:

- Graphs
  - Sample scripts to operate graphs with NetworkX
  - Sample scripts to operate graphs in Oracle Graph.
- Sets
  - Sample scripts to operate set in Python

Also, it will host the collection of memos for the following.

- References such as product/package/library documents.
- References to papers on arXivs or DOI.
- Memos that summarize the idea behind the code.

## Prerequisites

- Readers who want to use the Oracle Graph code must have a running instance of Oracle Database 19c or later.
- Readers need to use editors or viewers who can render Markdown, MathJax, and Mermaid for the best reading experience.

## Structure of This Project

The project has sections for Graph Theory and Set Theory.
Group Theory is not in the current scope.

```
├── graph
│  └── pgx
│     └── networkx (directory for networkx samples)
├── README.md
└── set
```

## Copyright

The respective owners own the copyright of the referenced documents and packages.
Their copyright follows the terms and conditions set by the licensors.

The code and memo in this repository are under GNU Affero General Public License (GPLv3).

* [LICENSE](LICENSE)
* For details, visit [GNU Affero General Public License.](https://www.gnu.org/licenses/agpl-3.0.en.html)

(End of README.md)
