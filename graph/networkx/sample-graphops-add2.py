#=========================================================================
#
#  Graph Calculation with NetworkX
#
#  Copyright August 2023 by Hisao Tsujimura
#
#   This is a sample NetworkX script to:
#       Demonstrate the graph operations (addition)
#       not using NetworkX method
#
#=========================================================================

import networkx as nx
import sys

G = nx.Graph()  # Undirected Graph
H = nx.Graph()  # Undirected Graph

# Add vertices (nodes) / edges (links) to each graph

# V(G) = { "a", "b", "c" }
g_elements = { "a", "b", "c" }
for v in g_elements:
    G.add_node(v)

# H(G) = { "b", "c", "d" }
h_elements = { "b", "c", "d" }
for v in h_elements:
    H.add_node(v)

# E(G) = { ab, bc }
G.add_edge("a", "b")
G.add_edge("b", "c")

# E(H) = { bc, cd }
H.add_edge("b", "c")
H.add_edge("c", "d")

print("=== Before ===")
print("## V(G)")
print(G.nodes.data())
print("## E(G)")
print(G.edges.data())

print("## V(H)")
print(H.nodes.data())
print("## E(H)")
print(H.edges.data())

# Add operation
G.add_nodes_from(H.nodes.data())
G.add_edges_from(H.edges.data())

print("=== After ===")
print("## V(G)")
print(G.nodes.data())
print("## E(G)")
print(G.edges.data())
