#=========================================================================
#
#  Graph Calculation with NetworkX
#
#  Copyright August 2023 by Hisao Tsujimura
#
#   This is a sample NetworkX script to:
#       Demonstrate the graph operations (difference)
#
#=========================================================================

import networkx as nx
import sys

G = nx.Graph()  # Undirected Graph
H = nx.Graph()  # Undirected Graph

# Define vertices (nodes) / edges (links) to each graph
# For difference, the set of nodes have to be the same.

# V(G) = { "a", "b", "c" }
# V(H) = { "a", "b", "c" }
elements = { "a", "b", "c" }
for v in elements:
    G.add_node(v)
    H.add_node(v)

# E(G) = { ab, bc }
G.add_edge("a", "b")
G.add_edge("b", "c")

# E(H) = { bc, cd }
H.add_edge("b", "c")

print("=== Before ===")
print("## V(G)")
print(G.nodes.data())
print("## E(G)")
print(G.edges.data())

print("## V(H)")
print(H.nodes.data())
print("## E(H)")
print(H.edges.data())

# Add operation
D = nx.difference(G,H)

print("=== Difference ===")
print("## V(G)")
print(D.nodes.data())
print("## E(G)")
print(D.edges.data())
